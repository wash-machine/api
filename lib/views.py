from service_order.models import (
   ServiceOrder,
   ServiceOrderState,
)

def priority_queue():
    state = ServiceOrderState.objects.get(name='Em espera')
    querryset = ServiceOrder.objects.filter(state=state)
    querryset = querryset.order_by('deadline', 'start_time')

    return querryset


def get_state_name(state):
    if state == '0':
        return 'Em espera'
    elif state == '1':
        return 'Escovando'
    elif state == '2':
        return 'Enxaguando'
    elif state == '3':
        return 'Secando'
    else:
        return 'InvalidState'
