import functools
import random
from wash_machine.validators import validate_cpf
from faker import Factory

FAKE_GENERATOR = Factory.create('pt_BR')


def gen_cpf(unused_factory):
    cpf = ""
    while cpf == "":
        try:
            cpf = validate_cpf(
                FAKE_GENERATOR.cpf()
            )
        except BaseException:
            pass

    return cpf
