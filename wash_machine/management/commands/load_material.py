from django.core.management.base import BaseCommand
from service_order.models import Material
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_materials(materials):
        for material in materials:
            Material.objects.get_or_create(
                **material)
            print('\tMaterial {name} was registered'
                  .format(**material))

    def handle(self, *args, **unused_kwargs):
        load_seed('material_seed.yml', self._seed_materials)
