from django.core.management.base import BaseCommand
from service_order.models import ShoeType
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_shoe_types(shoe_types):
        for shoe_type in shoe_types:
            ShoeType.objects.get_or_create(
                **shoe_type)
            print('\tShoeType {name} was registered'
                  .format(**shoe_type))

    def handle(self, *args, **unused_kwargs):
        load_seed('shoe_type_seed.yml', self._seed_shoe_types)
