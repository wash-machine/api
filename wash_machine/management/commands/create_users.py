from django.core.management.base import BaseCommand
from user_manager.factories import (
    ClientFactory, EmployeeFactory,
    ResidentialDataFactory
)
from user_manager.models import Employee


class Command(BaseCommand):


    def handle(self, *args, **kwargs):
        main_user, _ = Employee.objects.get_or_create(email="a@example.com",
                                     telephone='(61) 91234-5123',
                                     cpf='12312312387')
        main_user.set_password('123456')
        self.stdout.write("Default user password '123456' email 'a@example.com'")
        main_user.save()
        self.stdout.write("Create 5 client")
        ClientFactory.create_batch(5)
        self.stdout.write("Create 5 client")
        EmployeeFactory.create_batch(5)
        self.stdout.write("Create 5 employee")
