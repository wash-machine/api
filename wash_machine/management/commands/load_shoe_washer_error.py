from django.core.management.base import BaseCommand
from service_order.models import ShoeWasherError
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_errors(errors):
        for error in errors:
            ShoeWasherError.objects.get_or_create(
                **error)
            print('\tShoe washer error {name} was registered'
                  .format(**error))

    def handle(self, *args, **unused_kwargs):
        load_seed('shoe_washer_error_seed.yml', self._seed_errors)
