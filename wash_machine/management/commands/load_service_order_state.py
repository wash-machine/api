from django.core.management.base import BaseCommand
from service_order.models import ServiceOrderState
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_service_order_states(service_order_states):
        for service_order_state in service_order_states:
            ServiceOrderState.objects.get_or_create(
                **service_order_state)
            print('\tServiceOrderState {name} was registered'
                  .format(**service_order_state))

    def handle(self, *args, **unused_kwargs):
        load_seed('service_order_state_seed.yml',
            self._seed_service_order_states)
