from django.core.management.base import BaseCommand
from service_order.models import PaymentType
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_payment_types(payment_types):
        for payment_type in payment_types:
            PaymentType.objects.get_or_create(
                **payment_type)
            print('\tPaymentType {name} was registered'
                  .format(**payment_type))

    def handle(self, *args, **unused_kwargs):
        load_seed('payment_type_seed.yml', self._seed_payment_types)
