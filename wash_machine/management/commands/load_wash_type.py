from django.core.management.base import BaseCommand
from service_order.models import WashType
from .utils.load_seed import load_seed


class Command(BaseCommand):
    @staticmethod
    def _seed_wash_types(wash_types):
        for wash_type in wash_types:
            WashType.objects.get_or_create(
                **wash_type)
            print('\tWashType {name} was registered'
                  .format(**wash_type))

    def handle(self, *args, **unused_kwargs):
        load_seed('wash_type_seed.yml', self._seed_wash_types)
