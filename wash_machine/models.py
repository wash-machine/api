from django.db import models
from django.contrib.auth.models import User


class BaseModel(models.Model):

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.full_clean()

        super(BaseModel, self).save(*args, **kwargs)
