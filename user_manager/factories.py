import factory
from lib.factories import gen_cpf
from . import models
from service_order.factories import ServiceOrderFactory


class StateFactory(factory.DjangoModelFactory):

    class Meta:
        model = models.State

    name = factory.Faker('state')
    abbreviation = factory.Faker('state_abbr')


class ResidentialDataFactory(factory.DjangoModelFactory):

    class Meta:
        model = models.ResidentialData

    city = factory.Faker('city')
    address = factory.Faker('address')
    neighborhood = factory.Faker('street_name')
    cep = factory.Sequence(lambda n: '700000%s' % n)
    state = factory.SubFactory(StateFactory)
    number = factory.Faker('building_number')
    complement = factory.Faker('secondary_address')


class ClientFactory(factory.DjangoModelFactory):

    class Meta:
        model = models.Client

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.LazyAttribute(lambda o: '%s@example.org' % o.first_name)
    telephone = factory.Sequence(lambda n: '(61) 9 1234-%04d' % n)
    cpf = factory.LazyAttribute(gen_cpf)
    residential_data = factory.SubFactory(ResidentialDataFactory)
    service_order = factory.RelatedFactory(ServiceOrderFactory, 'client')


class EmployeeFactory(factory.DjangoModelFactory):

    class Meta:
        model = models.Employee

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.LazyAttribute(lambda o: '%s@example.org' % o.first_name)
    telephone = factory.Sequence(lambda n: '(61) 9 2234-%04d' % n)
    cpf = factory.LazyAttribute(gen_cpf)
