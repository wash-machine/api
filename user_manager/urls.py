from rest_framework import routers
from user_manager.views import (
    ClientViewSet,
    StateViewSet,
    EmployeeViewSet,
    ResidentialDataViewSet,
)


APP_NAME = 'user_manager'

ROUTER = routers.DefaultRouter()
ROUTER.register(r'^client', ClientViewSet)
ROUTER.register(r'^residential_data', ResidentialDataViewSet)
ROUTER.register(r'^state', StateViewSet)
ROUTER.register(r'^employee', EmployeeViewSet)

urlpatterns = ROUTER.urls
