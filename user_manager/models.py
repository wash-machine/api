from django.db import models
from django.contrib.auth.models import User
from wash_machine import models as base_models
from wash_machine import validators


class Employee(User):

    cpf = models.CharField(
        max_length=14,
        validators=[validators.validate_cpf]
    )

    telephone = models.CharField(
        max_length=16,
        validators=[validators.validate_phone_number]
    )

    @property
    def name(self):
        return self.first_name


    def save(self, *args, **kwargs):

        if not self.pk:
            if not self.password:
                self.password = 'default123'
            self.set_password(self.password)

        self.username = self.email
        self.is_superuser = True
        # self.full_clean() prevent errors in rest framework

        super(Employee, self).save(*args, **kwargs)


class ResidentialData(base_models.BaseModel):

    state = models.ForeignKey(
        'State',
        related_name='state'
    )

    address = models.CharField(
        max_length=70
    )

    neighborhood = models.CharField(
        max_length=30
    )

    cep = models.CharField(
        max_length=9
    )

    number = models.IntegerField()

    complement = models.CharField(
        max_length=20
    )

    city = models.CharField(
        max_length=50
    )

    def __str__(self):
        return "cep: {}, nº: {}".format(self.cep, self.number)


class Client(User):

    cpf = models.CharField(
        max_length=14,
        validators=[validators.validate_cpf]
    )

    telephone = models.CharField(
        max_length=19,
        validators=[validators.validate_phone_number]
    )  # considering +55

    residential_data = models.OneToOneField(
        ResidentialData,
        on_delete=models.CASCADE,
        related_name='client',
    )


    @property
    def name(self):
        return self.first_name


    def save(self, *args, **kwargs):

        if not self.pk:
            if not self.password:
                self.password = 'default123'
            self.set_password(self.password)

        self.username = self.email
        # self.full_clean() prevent errors in rest

        super(Client, self).save(*args, **kwargs)


class State(base_models.BaseModel):

    name = models.CharField(
        max_length=55
    )

    abbreviation = models.CharField(
        max_length=2
    )

    def __str__(self):
        return self.name
