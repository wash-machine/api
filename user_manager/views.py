from rest_framework import viewsets
from user_manager.serializers import (
    ClientSerializer, ResidentialDataSerializer,
    EmployeeSerializer, StateSerializer,
)
from user_manager.models import (
    Client, ResidentialData, Employee, State,
)


class ClientViewSet(viewsets.ModelViewSet):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()


class EmployeeViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    queryset = Employee.objects.all()


class ResidentialDataViewSet(viewsets.ModelViewSet):
    serializer_class = ResidentialDataSerializer
    queryset = ResidentialData.objects.all()


class StateViewSet(viewsets.ModelViewSet):
    serializer_class = StateSerializer
    queryset = State.objects.all()
