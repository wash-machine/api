from rest_framework import serializers
from django.contrib.auth.models import User
from user_manager.models import (
    ResidentialData,
    Client,
    Employee,
    State,
)


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'pk',
            'email',
            'first_name',
            'is_superuser',
        ]


class StateSerializer(serializers.ModelSerializer):

    class Meta:
        model = State
        fields = [
            'id',
            'name',
            'abbreviation',
        ]



class ResidentialDataSerializer(serializers.ModelSerializer):

    state_id = serializers.IntegerField()
    state = StateSerializer(read_only=True)

    class Meta:
        model = ResidentialData
        fields = [
            'id',
            'address',
            'city',
            'neighborhood',
            'cep',
            'number',
            'complement',
            'state_id',
            'state',
        ]


class ClientSerializer(serializers.ModelSerializer):
    residential_data = ResidentialDataSerializer(read_only=True)
    residential_data_id = serializers.IntegerField()

    class Meta:

        model = Client
        fields = [
            'id',
            'first_name',
            'last_name',
            'cpf',
            'telephone',
            'residential_data',
            'residential_data_id',
            'email',
            'is_superuser',
            'name',
        ]
        read_only_fields = ('name', )


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:

        model = Employee
        fields = [
            'id',
            'first_name',
            'last_name',
            'cpf',
            'telephone',
            'email',
            'is_superuser',
            'name',
        ]
        read_only_fields = ('name', )
