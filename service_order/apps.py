from django.apps import AppConfig


class ServiceOrderConfig(AppConfig):
    name = 'service_order'
