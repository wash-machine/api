from rest_framework import serializers
from user_manager.serializers import UserSerializer
from service_order.models import (
    ServiceOrder,
    PaymentType,
    Shoe,
    ShoeType,
    Material,
    ServiceOrderState,
    WashType,
    ShoeWasher,
    ShoeWasherError
)


class PaymentTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentType
        fields = [
            'id',
            'name',
        ]


class ShoeTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShoeType
        fields = [
            'id',
            'name',
        ]


class MaterialSerializer(serializers.ModelSerializer):

    class Meta:
        model = Material
        fields = [
            'id',
            'name',
        ]


class ShoeSerializer(serializers.ModelSerializer):

    shoe_type_data = ShoeTypeSerializer(read_only=True, source='shoe_type')
    material_data = MaterialSerializer(read_only=True, source='material')

    class Meta:
        model = Shoe
        fields = [
            'id',
            'shoe_type',
            'shoe_type_data',
            'material',
            'material_data',
        ]


class ServiceOrderStateSerializer(serializers.ModelSerializer):

    class Meta:
        model = ServiceOrderState
        fields = [
            'id',
            'name',
        ]


class WashTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = WashType
        fields = [
            'id',
            'name',
        ]


class ShoeWasherErrorSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShoeWasherError
        fields = [
            'id',
            'name',
            'description',
            'code',
        ]


class ShoeWasherSerializer(serializers.ModelSerializer):

    errors = ShoeWasherErrorSerializer(many=True, read_only=True)
    class Meta:
        model = ShoeWasher
        fields = [
            'id',
            'name',
            'is_busy',
            'errors',
        ]


class ServiceOrderSerializer(serializers.ModelSerializer):

    shoe = ShoeSerializer(read_only=True)
    shoe_id = serializers.IntegerField()
    wash_type_data = ServiceOrderStateSerializer(read_only=True,
        source='wash_type')
    state_data = ServiceOrderStateSerializer(read_only=True, source='state')
    payment_type_data = PaymentTypeSerializer(read_only=True, source='payment_type')
    client_data = UserSerializer(read_only=True, source='client')


    class Meta:
        model = ServiceOrder
        fields = [
            'id',
            'start_time',
            'deadline',
            'wash_end',
            'observation',
            'is_paid',
            'payment_value',
            'payment_type',
            'payment_type_data',
            'shoe',
            'shoe_id',
            'wash_type_data',
            'wash_type',
            'state_data',
            'state',
            'shoe_washer',
            'client_data',
            'client',
        ]
