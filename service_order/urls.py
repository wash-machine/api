from django.conf.urls import url
from rest_framework import routers
from service_order.views import (
    ServiceOrderViewSet,
    PaymentTypeViewSet,
    ShoeTypeViewSet,
    MaterialViewSet,
    ShoeViewSet,
    ServiceOrderPeriodicQueueList,
    ServiceOrderStateViewSet,
    WashTypeViewSet,
    ShoeWasherViewSet,
    ShoeWasherErrorAPI,
    ServiceOrderChangeState
)


APP_NAME = 'service_order'

ROUTER = routers.DefaultRouter()
ROUTER.register(r'^service_order', ServiceOrderViewSet)
ROUTER.register(r'^payment_type', PaymentTypeViewSet)
ROUTER.register(r'^shoe_type', ShoeTypeViewSet)
ROUTER.register(r'^material', MaterialViewSet)
ROUTER.register(r'^shoe', ShoeViewSet)
ROUTER.register(r'^service_order_state', ServiceOrderStateViewSet)
ROUTER.register(r'^wash_type', WashTypeViewSet)
ROUTER.register(r'^shoe_washer', ShoeWasherViewSet)

urlpatterns = ROUTER.urls + [
    url(r'^service_order_queue', ServiceOrderPeriodicQueueList.as_view()),
    url(r'^start_wash/(?P<pk_shoe_washer>[0-9]+)/$', ServiceOrderChangeState.start_wash),
    url(r'^get_errors/(?P<pk_shoe_washer>[0-9]+)/$', ShoeWasherErrorAPI.get_errors),
    url(r'^update_state/(?P<pk>[0-9]+)/(?P<state>[0-4]+)/$', ServiceOrderChangeState.update_state),
]
