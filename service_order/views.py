from rest_framework import (
    viewsets,
    generics,
    views,
    response,
    status
)
from rest_framework.decorators import (
    api_view,
    permission_classes
)
from rest_framework.permissions import AllowAny
from service_order.serializers import (
    ServiceOrderSerializer,
    PaymentTypeSerializer,
    ShoeTypeSerializer,
    MaterialSerializer,
    ShoeSerializer,
    ServiceOrderStateSerializer,
    WashTypeSerializer,
    ShoeWasherSerializer,
)
from service_order.models import (
    ServiceOrder,
    PaymentType,
    ShoeType,
    Material,
    Shoe,
    ServiceOrderState,
    WashType,
    ShoeWasher,
    ShoeWasherError,
)
from lib.views import (
    priority_queue,
    get_state_name
)
import json


class ServiceOrderChangeState(views.APIView):

    @api_view(["PUT"])
    @permission_classes((AllowAny, ))
    def start_wash(request, pk_shoe_washer=None):
            queue = priority_queue()
            service_order = queue.first()
            service_order.state = ServiceOrderState.objects.get(name='Escovando')
            service_order.shoe_washer = ShoeWasher.objects.get(pk=pk_shoe_washer)
            service_order.shoe_washer.is_busy = True

            service_order.shoe_washer.save()
            service_order.save()

            return response.Response(
                {'service_order_id': service_order.id},
                status=status.HTTP_200_OK
            )
            return response.Response(
                {'message': 'Não foi possível salvar a service_order, ou'
                    ' não foi encontrado nenhum objeto com pk referido ou'
                    ' nenhum ServiceOrderState.name=Molhando'
                    '{}'.format(pk_shoe_washer)},
                status=status.HTTP_400_BAD_REQUEST
            )


    @api_view(["PUT"])
    @permission_classes((AllowAny, ))
    def update_state(request, pk=None, state=None):
        try:
            service_order = ServiceOrder.objects.get(pk=pk)
            new_state = get_state_name(state)
            service_order.state = ServiceOrderState.objects.get(name=new_state)

            if state == "3":
                print('\n\nupdating to busy false\n\n')
                service_order.shoe_washer.is_busy = False
                service_order.shoe_washer.save()

            service_order.save()
            return response.Response(
                {'service_order_state': new_state},
                status=status.HTTP_200_OK
            )
        except Exception as error:
            return response.Response(
                {'message': '{}'.format(str(error))},
                status=status.HTTP_400_BAD_REQUEST
            )


class ShoeWasherErrorAPI(views.APIView):

    @api_view(["GET", "PUT"])
    @permission_classes((AllowAny, ))
    def get_errors(request, pk_shoe_washer=None):
        if request.method == 'PUT':
            error_code = json.loads(request.body.decode('utf-8'))
            error_code = error_code["error_code"]
            try:
                shoe_washer = ShoeWasher.objects.get(pk=pk_shoe_washer)
                error = ShoeWasherError.objects.get(code=error_code)
                shoe_washer.errors.add(error)
                return response.Response(
                    {'message': 'Erro submetido com sucesso'},
                    status=status.HTTP_204_NO_CONTENT
                )
            except Exception as error:
                return response.Response(
                    {'message': '{}'.format(error)},
                    status=status.HTTP_400_BAD_REQUEST
                )
        elif request.method == 'GET':
            shoe_washer = ShoeWasher.objects.get(pk=pk_shoe_washer)
            shoe_washer.errors.clear()
            return response.Response(
                {'message': 'Erros cleaned'},
                status=status.HTTP_200_OK
            )




class ServiceOrderViewSet(viewsets.ModelViewSet):
    serializer_class = ServiceOrderSerializer
    queryset = ServiceOrder.objects.all()


class ServiceOrderPeriodicQueueList(generics.ListAPIView):
    serializer_class = ServiceOrderSerializer

    def get_queryset(self):
        querryset = priority_queue()

        return querryset


class PaymentTypeViewSet(viewsets.ModelViewSet):
    serializer_class = PaymentTypeSerializer
    queryset = PaymentType.objects.all()


class ShoeTypeViewSet(viewsets.ModelViewSet):
    serializer_class = ShoeTypeSerializer
    queryset = ShoeType.objects.all()


class MaterialViewSet(viewsets.ModelViewSet):
    serializer_class = MaterialSerializer
    queryset = Material.objects.all()


class ShoeViewSet(viewsets.ModelViewSet):
    serializer_class = ShoeSerializer
    queryset = Shoe.objects.all()


class ServiceOrderStateViewSet(viewsets.ModelViewSet):
    serializer_class = ServiceOrderStateSerializer
    queryset = ServiceOrderState.objects.all()


class WashTypeViewSet(viewsets.ModelViewSet):
    serializer_class = WashTypeSerializer
    queryset = WashType.objects.all()


class ShoeWasherViewSet(viewsets.ModelViewSet):
    serializer_class = ShoeWasherSerializer
    queryset = ShoeWasher.objects.all()
