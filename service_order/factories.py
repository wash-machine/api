import datetime
import factory
import factory.fuzzy
from .models import *


class ShoeWasherFactory(factory.DjangoModelFactory):

    class Meta:
        model = ShoeWasher

    name = factory.Iterator(['Destruidora de Sujeiras', 'Melhor que Brastemp'])
    is_busy = factory.Iterator([True, False])


class ShoeFactory(factory.DjangoModelFactory):

    class Meta:
        model = Shoe

    shoe_type = factory.fuzzy.FuzzyChoice(ShoeType.objects.all())
    material = factory.fuzzy.FuzzyChoice(Material.objects.all())


class ServiceOrderFactory(factory.DjangoModelFactory):

    class Meta:
        model = ServiceOrder
        exclude = ('now',)

    now = datetime.datetime.now()
    is_paid = factory.Iterator([True, False])
    deadline = factory.fuzzy.FuzzyNaiveDateTime(now,
                                                now+datetime.timedelta(5))
    payment_value = factory.fuzzy.FuzzyDecimal(2.3, 124.9)
    payment_type = factory.fuzzy.FuzzyChoice(PaymentType.objects.all())
    state = factory.fuzzy.FuzzyChoice(ServiceOrderState.objects.all())
    wash_type = factory.fuzzy.FuzzyChoice(WashType.objects.all())
    shoe_washer = factory.SubFactory(ShoeWasherFactory)
    shoe = factory.SubFactory(ShoeFactory)
