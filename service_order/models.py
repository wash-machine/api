from wash_machine.models import BaseModel
from user_manager.models import Client
from django.db import models


class PaymentType(BaseModel):

    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class ShoeType(BaseModel):

    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class Material(BaseModel):

    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class Shoe(BaseModel):

    shoe_type = models.ForeignKey(
        ShoeType,
        related_name='shoe'
    )

    material = models.ForeignKey(
        Material,
        related_name='shoe'
    )

    def __str__(self):
        return "ShoeType={}, material={}".format(self.shoe_type, self.material)


class ServiceOrderState(BaseModel):

    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class WashType(BaseModel):

    name = models.CharField(
        max_length=100
    )

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class ShoeWasher(BaseModel):

    name = models.CharField(
        max_length=100
    )

    is_busy = models.BooleanField()

    

    def __str__(self):
        return "Name={name}".format(**self.__dict__)


class ShoeWasherError(BaseModel):

    name = models.CharField(
        max_length=100
    )

    description = models.CharField(
        max_length=255
    )

    shoe_washers = models.ManyToManyField(
        ShoeWasher,
        related_name='errors',
    )

    code = models.IntegerField()

    def __str__(self):
        return "Code={code} Name={name}".format(**self.__dict__)


class ServiceOrder(BaseModel):

    client = models.ForeignKey(
        Client,
        related_name='service_orders',
    )

    payment_type = models.ForeignKey(
        PaymentType,
        related_name='service_orders'
    )

    shoe = models.ForeignKey(
        Shoe,
        related_name='service_orders',
        on_delete=models.CASCADE
    )

    state = models.ForeignKey(
        ServiceOrderState,
        related_name='service_orders'
    )

    wash_type = models.ForeignKey(
        WashType,
        related_name='service_orders'
    )

    shoe_washer = models.ForeignKey(
        ShoeWasher,
        related_name='service_orders',
        blank=True,
        null=True,
    )

    start_time = models.DateTimeField(
        auto_now_add=True
    )

    wash_end = models.DateTimeField(
        null=True,
        blank=True
    )

    observation = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )

    payment_value = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )

    deadline = models.DateTimeField()

    is_paid = models.BooleanField(default=False)

    def __str__(self):
        return "Identifier={id}".format(**self.__dict__)
